#include <math.h>
#include <mpi.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <iostream>
using namespace std;

typedef struct
{
    void *data;

    int type;
    int used;
    int allocated;
    int item_size;
} array_t;

/* Internal use only. */
void *array_get_data(array_t *array)
{
    return (char *)array->data + sizeof(void *);
}
array_t *array_get_ptr(void *array_data)
{
    return (array_t *)(*(uintptr_t *)((char *)array_data - sizeof(void *)));
}
void *array_new(int size, int item_size)
{
    array_t *array = (array_t *)calloc(sizeof(*array), 1);
    if (array == NULL)
    {
        cout << "Failed to allocate " << sizeof(*array) << " bytes." << endl;
        exit(4);
    }
    if (size == -1)
    {
        size = 0;
        array->type = -1;
    }
    size_t allocate = (size_t)sizeof(void *) + size * item_size;
    array->data = calloc(allocate, 1);
    if (array->data == NULL)
    {
        cout << "Failed to allocate " << allocate << " bytes." << endl;
        exit(4);
    }

    array->used = 0;
    array->allocated = size;
    array->item_size = item_size;

    *(uintptr_t *)array->data = (uintptr_t)array;

    return array_get_data(array);
}

void array_concatenate(void *result_data_ptr, void *array1_data, void *array2_data)
{
    void **result_data = (void **)result_data_ptr;

    array_t *result = array_get_ptr(*result_data);
    array_t *array1 = array_get_ptr(array1_data);
    array_t *array2 = array_get_ptr(array2_data);

    int item_size = result->item_size;
    size_t allocate = (size_t)sizeof(void *) + (array1->allocated + array2->allocated) * item_size;
    result->data = realloc(result->data, allocate);
    if (result->data == NULL)
    {
        cout << "Failed to allocate " << allocate << " bytes." << endl;
        exit(4);
    }

    memcpy((char *)result->data + sizeof(void *),
           array1_data, array1->allocated * item_size);
    memcpy((char *)result->data + sizeof(void *) + array1->allocated * item_size,
           array2_data, array2->allocated * item_size);

    result->allocated = array1->allocated + array2->allocated;
    *result_data = array_get_data(result);
}

void array_copy(void *destination_data, void *source_data, int first, int count)
{
    int item_size = array_get_ptr(destination_data)->item_size;
    memcpy(destination_data, (char *)source_data + first * item_size, count * item_size);
}

void array_delete(void *array_data_ptr)
{
    void **array_data = (void **)array_data_ptr;
    array_t *array = array_get_ptr(*array_data);
    free(array->data);
    free(array);
    *array_data = NULL;
}

/* Internal use only. */
void array_expand(void *array_data_ptr)
{
    void **array_data = (void **)array_data_ptr;
    array_t *array = array_get_ptr(*array_data);
    if (array->used < array->allocated)
    {
        return;
    }
    if (array->allocated == 0)
    {
        array->allocated = 16;
    }
    else
    {
        array->allocated *= 2;
    }
    size_t allocate = (size_t)sizeof(void *) + array->allocated * array->item_size;
    array->data = realloc(array->data, allocate);
    if (array->data == NULL)
    {
        cout << "Failed to allocate " << allocate << " bytes." << endl;
        exit(4);
    }
    *array_data = array_get_data(array);
}

/* Use: int item_size = array_item_size(array). */
int array_item_size(void *array_data)
{
    return array_get_ptr(array_data)->item_size;
}

/* Use: array_push(&array, value). */
#define array_push(array_data, value)                              \
    do                                                             \
    {                                                              \
        array_expand(array_data);                                  \
        (*array_data)[array_get_ptr(*array_data)->used++] = value; \
    } while (0)

/* Use: int size = array_size(array). */
int array_size(void *array_data)
{
    array_t *array = array_get_ptr(array_data);
    if (array->type == -1)
    {
        return array->used;
    }
    else
    {
        return array->allocated;
    }
}

typedef struct
{
    int a;
    int b;
} pair_t;
pair_t *comparators;
#define clocktimeDifference(start, stop) \
    1.0 * (stop.tv_sec - start.tv_sec) + \
        1.0 * (stop.tv_nsec - start.tv_nsec) / 1E+9

int compare_int(const void *a, const void *b)
{
    if (*(int *)a < *(int *)b)
        return -1;
    if (*(int *)a == *(int *)b)
        return 0;
    return 1;
}

void swap_ptr(void *ptr1_ptr, void *ptr2_ptr)
{
    void **ptr1 = (void **)ptr1_ptr;
    void **ptr2 = (void **)ptr2_ptr;

    void *tmp = *ptr1;
    *ptr1 = *ptr2;
    *ptr2 = tmp;
}

void S(int *procs_up, int *procs_down)
{
    int proc_count = array_size(procs_up) + array_size(procs_down);
    if (proc_count == 1)
    {
        return;
    }
    else if (proc_count == 2)
    {
        array_push(&comparators, ((pair_t){procs_up[0], procs_down[0]}));
        return;
    }

    int *procs_up_odd = (int *)array_new(array_size(procs_up) / 2 + array_size(procs_up) % 2, sizeof(int));
    int *procs_down_odd = (int *)array_new(array_size(procs_down) / 2 + array_size(procs_down) % 2, sizeof(int));
    int *procs_up_even = (int *)array_new(array_size(procs_up) / 2, sizeof(int));
    int *procs_down_even = (int *)array_new(array_size(procs_down) / 2, sizeof(int));
    int *procs_result = (int *)array_new(array_size(procs_up) + array_size(procs_down), sizeof(int));

    for (int i = 0; i < array_size(procs_up); i++)
    {
        if (i % 2)
        {
            array_push(&procs_up_even, procs_up[i]);
        }
        else
        {
            array_push(&procs_up_odd, procs_up[i]);
        }
    }
    for (int i = 0; i < array_size(procs_down); i++)
    {
        if (i % 2)
        {
            array_push(&procs_down_even, procs_down[i]);
        }
        else
        {
            array_push(&procs_down_odd, procs_down[i]);
        }
    }

    S(procs_up_odd, procs_down_odd);
    S(procs_up_even, procs_down_even);

    array_concatenate(&procs_result, procs_up, procs_down);

    for (int i = 1; i + 1 < array_size(procs_result); i += 2)
    {
        array_push(&comparators, ((pair_t){procs_result[i], procs_result[i + 1]}));
    }

    array_delete(&procs_up_odd);
    array_delete(&procs_down_odd);
    array_delete(&procs_up_even);
    array_delete(&procs_down_even);
    array_delete(&procs_result);
}

void B(int *procs)
{
    if (array_size(procs) == 1)
    {
        return;
    }

    int *procs_up = (int *)array_new(array_size(procs) / 2, sizeof(int));
    int *procs_down = (int *)array_new(array_size(procs) / 2 + array_size(procs) % 2, sizeof(int));

    array_copy(procs_up, procs, 0, array_size(procs_up));
    array_copy(procs_down, procs, array_size(procs_up), array_size(procs_down));

    B(procs_up);
    B(procs_down);
    S(procs_up, procs_down);

    array_delete(&procs_up);
    array_delete(&procs_down);
}

void batcher(int proc_count)
{
    int *procs = (int *)array_new(proc_count, sizeof(int));
    for (int i = 0; i < array_size(procs); i++)
    {
        procs[i] = i;
    }
    B(procs);
    array_delete(&procs);
}

int main(int argc, char *argv[])
{
    if (argc != 3)
    {
        cout << "Usage: batcher <input.dat> <output.dat>." << endl;
        return 1;
    }

    MPI_Status status;
    double sort_time;

    MPI_Init(&argc, &argv);

    int rank, proc_count;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &proc_count);

    if (proc_count < 1)
    {
        cout << "Processors count must be > 1." << endl;
        MPI_Finalize();
        return 1;
    }

    comparators = (pair_t *)array_new(-1, sizeof(pair_t));
    batcher(proc_count);

    MPI_File input;
    if (MPI_File_open(MPI_COMM_WORLD, argv[1], MPI_MODE_RDONLY, MPI_INFO_NULL, &input) != MPI_SUCCESS)
    {
        if (rank == 0)
            printf("File '%s' can't be opened.\n", argv[1]);
        MPI_Finalize();
        return 2;
    }
    int elems_count;
    MPI_File_read(input, &elems_count, 1, MPI_UNSIGNED, &status);

    int elems_count_new = elems_count + (elems_count % proc_count ? proc_count - elems_count % proc_count : 0);
    int elems_per_proc_count = elems_count_new / proc_count;

    int *elems_result = (int *)array_new(elems_per_proc_count, sizeof(int));
    int *elems_current = (int *)array_new(elems_per_proc_count, sizeof(int));
    int *elems_temp = (int *)array_new(elems_per_proc_count, sizeof(int));

    MPI_File_seek_shared(input, sizeof(elems_count), MPI_SEEK_SET);
    MPI_File_read_ordered(input, elems_result, elems_per_proc_count, MPI_UNSIGNED, &status);

    MPI_File_close(&input);
    MPI_Barrier(MPI_COMM_WORLD);
    struct timespec start, stop;
    clock_gettime(CLOCK_MONOTONIC, &start);

    qsort(elems_result, elems_per_proc_count, array_item_size(elems_result), compare_int);
    if (proc_count > 1)
    {
        for (int i = 0; i < array_size(comparators); i++)
        {
            pair_t comparator = comparators[i];
            if (rank == comparator.a)
            {
                MPI_Send(elems_result, elems_per_proc_count, MPI_UNSIGNED, comparator.b, 0, MPI_COMM_WORLD);
                MPI_Recv(elems_current, elems_per_proc_count, MPI_UNSIGNED, comparator.b, 0, MPI_COMM_WORLD, &status);

                for (int res_index = 0, cur_index = 0, tmp_index = 0; tmp_index < elems_per_proc_count; tmp_index++)
                {
                    int result = elems_result[res_index];
                    int current = elems_current[cur_index];
                    if (result < current)
                    {
                        elems_temp[tmp_index] = result;
                        res_index++;
                    }
                    else
                    {
                        elems_temp[tmp_index] = current;
                        cur_index++;
                    }
                }
                swap_ptr(&elems_result, &elems_temp);
            }
            else if (rank == comparator.b)
            {
                MPI_Recv(elems_current, elems_per_proc_count, MPI_UNSIGNED, comparator.a, 0, MPI_COMM_WORLD, &status);
                MPI_Send(elems_result, elems_per_proc_count, MPI_UNSIGNED, comparator.a, 0, MPI_COMM_WORLD);

                int start = elems_per_proc_count - 1;
                for (int res_index = start, cur_index = start, tmp_index = start; tmp_index >= 0; tmp_index--)
                {
                    int result = elems_result[res_index];
                    int current = elems_current[cur_index];
                    if (result > current)
                    {
                        elems_temp[tmp_index] = result;
                        res_index--;
                    }
                    else
                    {
                        elems_temp[tmp_index] = current;
                        cur_index--;
                    }
                }
                swap_ptr(&elems_result, &elems_temp);
            }
        }
        MPI_Barrier(MPI_COMM_WORLD);
    }
    clock_gettime(CLOCK_MONOTONIC, &stop);
    if (rank == 0)
    {
        cout << "Elapsed time:" << clocktimeDifference(start, stop) << endl;
    }

    MPI_File output;
    if (MPI_File_open(MPI_COMM_WORLD, argv[2], MPI_MODE_CREATE | MPI_MODE_WRONLY, MPI_INFO_NULL, &output) != MPI_SUCCESS)
    {
        if (rank == 0)
            printf("File '%s' can't be opened.\n", argv[2]);
        MPI_Finalize();
        return 3;
    }
    MPI_File_set_size(output, 0);

    int skip = elems_count_new - elems_count;
    int print_offset = (skip / elems_per_proc_count == rank) * (skip % elems_per_proc_count);
    int print_count = (skip / elems_per_proc_count <= rank) * elems_per_proc_count - print_offset;

    MPI_File_write_ordered(output, &elems_count, rank == 0, MPI_UNSIGNED, &status);
    MPI_File_write_ordered(output, (unsigned char *)elems_result + print_offset * array_item_size(elems_result), print_count, MPI_UNSIGNED, &status);

    MPI_File_close(&output);

    array_delete(&comparators);
    array_delete(&elems_result);
    array_delete(&elems_current);
    array_delete(&elems_temp);

    MPI_Finalize();

    return 0;
}